package parser.atlassian.hoasung.com.atlassianparser.util;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import parser.atlassian.hoasung.com.atlassianparser.model.JSONChatMessage;
import parser.atlassian.hoasung.com.atlassianparser.model.Link;
import parser.atlassian.hoasung.com.atlassianparser.util.UrlHelper;

/**
 *
 * a helper to parse Chat message string to json format
 *
 * To use, simple call AtlassianChatMessageParser.parse( chatMessageText )
 *
 * @author thult304 <thult304@yahoo.com>
 *
 */
public class AtlassianChatMessageParser {

    /**
     * Contains patterns to parse chat message
     *
     */
    private static class MyPattern {
        final static String MENTION_REGEX = "@\\w+";

        final static String EMOTION_REGEX = "[(]\\p{Alnum}{1,15}[)]";

        final static Pattern MENTION_PATTERN = Pattern.compile(MENTION_REGEX);

        final static Pattern EMOTION_PATTERN = Pattern.compile(EMOTION_REGEX);

        final static Pattern WEB_URL_PATTERN = android.util.Patterns.WEB_URL;
    }

    /**
     * Parse a chat message string and returns a JSON string containing information about its contents.
     * Content to look for includes:
     * <p/>
     * 1. @mentions - A way to mention a user. Always starts with an '@' and ends when hitting a non-word character.
     * (http://help.hipchat.com/knowledgebase/articles/64429-how-do-mentions-work-)
     * 2. Emoticons - 'custom' emoticons which are alphanumeric strings,
     * no longer than 15 characters, contained in parenthesis. Assume that anything matching this format is an emoticon.
     * (https://www.hipchat.com/emoticons)
     * 3. Links - Any URLs contained in the message, along with the page's title.
     *
     * Note: this function is run on the called-thread.
     *
     * To avoid block UI and error happen, We should call this function in a background thread instead of UI Main Thread.
     *
     * Todo:Parsing title of a url is very slow so we should do it in another thread. For simple we only do this task int the same one thread.
     *
     * @param chatMessage message text need to parse
     *
     * @return returns a JSON string containing information: emotions, links and mentions
     *
     */
    public static String parse(String chatMessage) {

        JSONChatMessage jsonChatMessage = new JSONChatMessage();

        /* parse mentions */

        Matcher mentionMatcher = MyPattern.MENTION_PATTERN.matcher(chatMessage);
        while (mentionMatcher.find()) {

            if (jsonChatMessage.mentions == null) {
                jsonChatMessage.mentions = new ArrayList<>();
            }
            jsonChatMessage.mentions.add(getMentionPerson(mentionMatcher.group()));
        }


        /* parse emotions */

        Matcher emotionMatcher = MyPattern.EMOTION_PATTERN.matcher(chatMessage);

        while (emotionMatcher.find()) {

            String emotionName = emotionMatcher.group();

            if( isEmotionNameValid(emotionName)) {

                if (jsonChatMessage.emoticons == null) {
                    jsonChatMessage.emoticons = new ArrayList<>();
                }
                jsonChatMessage.emoticons.add(getEmotionName(emotionName));
            }
        }


        /* parse links */
        Matcher linkMatcher = MyPattern.WEB_URL_PATTERN.matcher(chatMessage);
        while (linkMatcher.find()) {

            if (jsonChatMessage.links == null) {
                jsonChatMessage.links = new ArrayList<>();
            }

            Link link = new Link();
            link.url = linkMatcher.group();
            try {
                link.title = UrlHelper.getUrlTitle(link.url);
            } catch (Exception e) {
                /* we can use a callback to send error to client.
                * for simple we just print error log here */
                e.printStackTrace();
            }

            jsonChatMessage.links.add(link);
        }

        return new Gson().toJson(jsonChatMessage);
    }

    /**
     * Check weather or not an emotionName is in the pre-defined emotion list
     *
     * for simple, we assume that every emotion is valid.
     *
     * @param emotionName
     * @return True if emotion in the pre-defined list otherwise false
     */
    private static boolean isEmotionNameValid(String emotionName) {

        //Todo: in the real application, we check emotionName is in the list: https://www.hipchat.com/emoticons

        return true;
    }

    /**
     * Remove characters ( and ) out of emotion pattern.
     * Example (success) -> success
     *
     * @param emotionPattern
     * @return emotion name without characters ( and )
     */
    private static String getEmotionName(String emotionPattern) {
        /* we don't need to check error because emotionPattern always has at least 3 chars */
        return emotionPattern.substring(1, emotionPattern.length() - 1);
    }

    /**
     * remove @ from person pattern
     * <p/>
     * example @thult -> thult
     *
     * @param personPattern
     * @return real name of person from person pattern
     */
    private static String getMentionPerson(String personPattern) {
        /* we don't need to check error because personPattern always has at least 2 chars */
        return personPattern.substring(1);
    }

}
