package parser.atlassian.hoasung.com.atlassianparser;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import parser.atlassian.hoasung.com.atlassianparser.util.AtlassianChatMessageParser;

/**
 * This class only helps to test parsing chat message text.
 *
 * it is not an actual application.
 *
 * @author thult304 <thult304@yahoo.com>
 */
public class MainActivity extends AppCompatActivity {

    private static final String SAMPLE_CHAT_TEXT = "@bob @john (success) such a cool feature; https://twitter.com/jdorfman/status/430511497475670016";

    @Bind(R.id.inputEditText)
    EditText inputEditText;

    @Bind(R.id.resultTextView)
    TextView resultTextView;

    @Bind(R.id.progressBar)
    View parsingProgressBar;

    @Bind(R.id.parseButton)
    View parseButton;

    private boolean mWasDestroyed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mWasDestroyed = false;

        ButterKnife.bind(this);
    }


    @OnClick(R.id.useSampleButton)
    void onUseSampleClicked() {
        inputEditText.setText(SAMPLE_CHAT_TEXT);
    }

    @OnClick(R.id.clearButton)
    void onClearClicked() {
        inputEditText.setText("");
        resultTextView.setText("");
    }

    @OnClick(R.id.parseButton)
    void onParseClicked() {

        parsingProgressBar.setVisibility(View.VISIBLE);
        parseButton.setVisibility(View.INVISIBLE);

        new Thread(){
            @Override
            public void run() {
                final String jsonChat = AtlassianChatMessageParser.parse(inputEditText.getText().toString());

                if(mWasDestroyed){
                    return;
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        parsingProgressBar.setVisibility(View.INVISIBLE);
                        parseButton.setVisibility(View.VISIBLE);
                        resultTextView.setText(jsonChat);
                    }
                });
            }
        }.start();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mWasDestroyed = true;
    }
}
