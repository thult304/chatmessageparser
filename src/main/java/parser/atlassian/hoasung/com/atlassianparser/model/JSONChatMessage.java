package parser.atlassian.hoasung.com.atlassianparser.model;

import java.util.List;

/**
 * Created by thult304 on 19/7/15.
 */
public class JSONChatMessage {
    public List<String> mentions;
    public List<String> emoticons;
    public List<Link> links;
}
