package parser.atlassian.hoasung.com.atlassianparser.util;

import org.jsoup.Jsoup;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

/**
 * Created by thult304 on 19/7/15.
 */
public class UrlHelper {
    public static String getUrlTitle(String url) throws IOException {
        if (url.startsWith("http://") || url.startsWith("https://")) {
            return Jsoup.connect(url).get().title();
        }

        return Jsoup.connect("http://" + url).get().title();
    }
}
